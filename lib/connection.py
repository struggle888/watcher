import socket
import threading
import time
import struct
import traceback

DEFAULT_PORT = 23456


class FileSyncConnections:
    def __init__(self, addresses: list, host='0.0.0.0', port=DEFAULT_PORT, max_client=5):

        self.client_connections = {}
        self._passive_handler = {}

        threading.Thread(target=self._build_server_connections, args=(host, port, max_client), daemon=True).start()
        threading.Thread(target=self._build_client_connections, args=(addresses,), daemon=True).start()

    def set_passive_handler(self, flag: int, func):
        self._passive_handler[flag] = func

    def client_send(self, client, buf):
        try:
            client.sendall(buf)
        except:
            print("Error")
            self._client_close(client)
            return False

    def client_recv(self, client, size):
        try:
            buf = b''
            while len(buf) < size:
                b = client.recv(size-len(buf))
                assert b is not None and len(b) > 0
                buf += b
            return buf
        except:
            traceback.print_exc()
            self._client_close(client)
            return False

    def client_send_with_size(self, client, buf):
        return self.client_send(client, struct.pack('>I', len(buf))) is not False and self.client_send(client, buf)

    def client_recv_with_size(self, client):
        length = self.client_recv(client, 4)
        if length is False:
            return False
        try:
            length = struct.unpack('>I', length)[0]
        except:
            raise
        return self.client_recv(client, length)

    def _client_close(self, client):
        try:
            client.close()
        except:
            pass
        finally:
            for address in self.client_connections:
                if self.client_connections[address] == client:
                    del self.client_connections[address]
                    threading.Thread(target=self._build_client_connections, args=([address],), daemon=True).start()
                    break

    def _connection_handler(self, client, address):
        while True:
            buf = self.client_recv(client, 1)
            if buf is False:
                print("Server connection abort:", address)
                threading.Thread(target=self._build_client_connections, args=([address[0]],), daemon=True).start()
                break
            if len(buf) == 0:  # connection close by remote side
                threading.Thread(target=self._build_client_connections, args=([address[0]],), daemon=True).start()
                break
            if buf[0] in self._passive_handler:
                self._passive_handler[buf[0]](client, address)

    def _build_client_connections(self, addresses: list):
        for address in addresses:
            self.client_connections[address] = self._build_client_connection(address)

    def _build_server_connections(self, host, port, max_client):
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        print("Bind on %s:%d" % (host, port))
        sock.bind((host, port))
        sock.listen(max_client)
        while True:
            client, address = sock.accept()
            threading.Thread(target=self._connection_handler, args=(client, address), daemon=True).start()

    @staticmethod
    def _build_client_connection(target):
        k = target.find(":")
        if k != -1:
            host = target[:k]
            port = target[k + 1:]
        else:
            host = target
            port = DEFAULT_PORT
        while True:
            try:
                sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                # 设置要连接的服务器的ip号和端口号
                sock.connect((host, port))
                return sock
            except:
                print("Connect to %s Error, retry in 1 second..." % target)
                time.sleep(1)
