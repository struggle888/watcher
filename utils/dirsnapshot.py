import os
from utils.utils import get_file_md5


def directory_snapshot(path):
    if not path.endswith("/"):
        path += "/"
    result = {
        "dirs": [],
        "files": []
    }
    for root, dirs, files in os.walk(path):
        for d in dirs:
            result["dirs"].append({"name": d, "parent": root[len(path):]})
        for f in files:
            result["files"].append({"name": f, "hash": get_file_md5(os.path.join(root, f)), "dir": root[len(path):],
                                    "size": os.path.getsize(os.path.join(root, f))})
    return result


def snapshot_diff(a, b):
    result = {
        "dirs": list(set([os.path.join(d["parent"], d["name"]) for d in a["dirs"]]) - set(
            [os.path.join(d["parent"], d["name"]) for d in b["dirs"]])),
        "files": {"create": [], "update": []}
    }
    a_files = {os.path.join(f["dir"], f["name"]): (f["hash"], f["size"]) for f in a["files"]}
    b_files = {os.path.join(f["dir"], f["name"]): (f["hash"], f["size"]) for f in b["files"]}
    for f in a_files:
        if f not in b_files:
            result["files"]["create"].append(f)
        elif b_files[f][0] != a_files[f][0] and b_files[f][1] <= a_files[f][1]:
            result["files"]["update"].append(f)
    return result


def has_diff(diff):
    return diff["dirs"] or diff["files"]["create"] or diff["files"]["update"]
