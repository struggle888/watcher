import hashlib


def get_file_md5(path):
    md5 = hashlib.md5()
    with open(path, 'rb') as f:
        while True:
            chunk = f.read(32768)
            if not chunk:
                break
            md5.update(chunk)
    return md5.hexdigest()

