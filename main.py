from utils.dirsnapshot import directory_snapshot, snapshot_diff, has_diff
from lib.connection import FileSyncConnections
import json
import os
import struct
import time
import threading
import hashlib
import argparse
import zlib


class LEFTFileSharer:
    def __init__(self, addresses, root="./share/"):
        # Status
        # IDLE: 0
        # Be SYNCed: 1
        # SYNCing: 2
        self._status = 0
        self.root = root
        self.syncing = []
        self._connection = FileSyncConnections(addresses=addresses)
        self._connection.set_passive_handler(1, self._sync_from)
        self._connection.set_passive_handler(2, self._get_snapshot)
        self._connection.set_passive_handler(3, self._request_sync)
        self.tasks = [{"type": "sync_from", "data": address} for address in addresses]
        threading.Thread(target=self.active_sync, daemon=True).start()

    def run_monitor(self):
        self.dir_monitor()

    def active_sync(self):
        while True:
            if not self.tasks:
                time.sleep(0.1)
                continue
            task = self.tasks.pop(0)
            if task["data"] not in self._connection.client_connections or task["data"] in self.syncing:
                self.tasks.append(task)
                time.sleep(0.1)
                continue
            if task["type"] == "sync_from":
                client = self._connection.client_connections[task["data"]]
                self._connection.client_send(client, b'\x02')
                target_snapshot = json.loads(self._connection.client_recv_with_size(client).decode())
                self_snapshot = directory_snapshot(self.root)
                diff = snapshot_diff(target_snapshot, self_snapshot)
                if has_diff(diff):
                    print("Has Diff, Sync from", task["data"])
                    self._connection.client_send(client, b'\x03')
                else:
                    print("Synced!")
            elif task["type"] == "sync_to":
                self._connection.client_send(self._connection.client_connections[task["data"]], b'\x01')
                if self._connection.client_recv(self._connection.client_connections[task["data"]], 1)[0] == 97:
                    print("Sync to %s" % task["data"])
                    self.syncing.append(task["data"])
                    threading.Thread(target=self._sync_to, args=(self._connection.client_connections[task["data"]], task["data"]),
                                     daemon=True).start()

    def _get_snapshot(self, client, address):
        self._connection.client_send_with_size(client, json.dumps(directory_snapshot(self.root)).encode())

    def _request_sync(self, client, address):
        print("Get Sync Request")
        self.tasks.append({"type": "sync_to", "data": address[0]})

    def _sync_from(self, client, address):
        if self._status == 0:
            self._status = 1
            self._connection.client_send(client, b'a')
            target_snapshot = json.loads(self._connection.client_recv_with_size(client).decode())
            self_snapshot = directory_snapshot(self.root)
            diff = snapshot_diff(target_snapshot, self_snapshot)
            while True:
                for d in diff["dirs"]:
                    os.makedirs(os.path.join(self.root, d), exist_ok=True)
                for f in diff["files"]["create"]:
                    self._connection.client_send_with_size(client, f.encode())
                    with open(self.root + f, "wb") as wf:
                        length = struct.unpack('>I', self._connection.client_recv(client, 4))[0]
                        while length > 0:
                            compressed = self._connection.client_recv(client, length)
                            buf = zlib.decompress(compressed)
                            wf.write(buf)
                            length = struct.unpack('>I', self._connection.client_recv(client, 4))[0]
                self._connection.client_send_with_size(client, b'\x00')
                for f in diff["files"]["update"]:
                    self._connection.client_send_with_size(client, f.encode())
                    with open(self.root + f, "rb+") as rf:
                        hashes = []
                        while True:
                            b = rf.read(1024*1024)
                            if b:
                                hashes.append(hashlib.md5(b).hexdigest())
                            else:
                                break
                        self._connection.client_send_with_size(client, json.dumps(hashes).encode())
                        index = struct.unpack('>i', self._connection.client_recv(client, 4))[0]
                        while index != -1:
                            buf = self._connection.client_recv_with_size(client)
                            buf = zlib.decompress(buf)
                            if index == -2:
                                rf.seek(0, 2)
                            else:
                                rf.seek(1024*1024*index, 0)
                            rf.write(buf)
                            index = struct.unpack('>i', self._connection.client_recv(client, 4))[0]
                self._connection.client_send_with_size(client, b'\x00')
                target_snapshot = json.loads(self._connection.client_recv_with_size(client).decode())
                self_snapshot = directory_snapshot(self.root)
                diff = snapshot_diff(target_snapshot, self_snapshot)
                if has_diff(diff):
                    import pprint
                    pprint.pprint(diff)
                    self._connection.client_send(client, b'\x01')
                else:
                    self._connection.client_send(client, b'\x00')
                    break
            print("Sync Done! [Receive]")
            self._status = 0
        else:
            self._connection.client_send(client, b'\x00')

    def _sync_to(self, client, address):
        try:
            self._status = 2
            self._connection.client_send_with_size(client, json.dumps(directory_snapshot(self.root)).encode())
            while True:
                # create file
                filepath = self._connection.client_recv_with_size(client)
                while filepath[0] != 0:
                    with open(self.root + filepath.decode(), "rb") as f:
                        b = f.read(1024 * 1024)
                        print("read!")
                        while b:
                            compressed = zlib.compress(b, 9)
                            self._connection.client_send_with_size(client, compressed)
                            b = f.read(1024 * 1024)
                        self._connection.client_send(client, struct.pack(">I", 0))
                    filepath = self._connection.client_recv_with_size(client)
                # update file
                filepath = self._connection.client_recv_with_size(client)
                while filepath[0] != 0:
                    with open(self.root + filepath.decode(), "rb") as f:
                        hashes = []
                        while True:
                            b = f.read(1024 * 1024)
                            if b:
                                hashes.append(hashlib.md5(b).hexdigest())
                            else:
                                break

                        from_hashes = json.loads(self._connection.client_recv_with_size(client).decode())
                        for index, h in enumerate(hashes):
                            if index > len(from_hashes) - 1:
                                self._connection.client_send(client, struct.pack(">i", -2))
                                f.seek(1024 * 1024 * index, 0)
                                buf = f.read(1024 * 1024)
                                compressed = zlib.compress(buf, 9)
                                self._connection.client_send_with_size(client, compressed)
                            elif from_hashes[index] != h:
                                self._connection.client_send(client, struct.pack(">i", index))
                                f.seek(1024*1024*index, 0)
                                buf = f.read(1024*1024)
                                compressed = zlib.compress(buf, 9)
                                self._connection.client_send_with_size(client, compressed)
                        self._connection.client_send(client, struct.pack(">i", -1))
                    filepath = self._connection.client_recv_with_size(client)
                #
                self._connection.client_send_with_size(client, json.dumps(directory_snapshot(self.root)).encode())
                if self._connection.client_recv(client, 1)[0] == 0:
                    break
            self._status = 0
            print("Sync Done! [Send]")
        finally:
            self.syncing.remove(address)

    def dir_monitor(self):
        current_snapshot = directory_snapshot(self.root)
        while True:
            time.sleep(0.1)
            if self._status == 0:
                now_snapshot = directory_snapshot(self.root)
                if has_diff(snapshot_diff(now_snapshot, current_snapshot)):
                    print("File Changed!")
                    current_snapshot = now_snapshot
                    for address in self._connection.client_connections:
                        self.tasks.append({"type": "sync_to", "data": address})


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--ip", type=str, default="")
    args = parser.parse_args()
    sharer = LEFTFileSharer(args.ip.split(","))
    sharer.run_monitor()
